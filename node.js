let obj ={
    especie : 'chachorro',
    nome: 'toto'
}
console.log('o'+obj.especie+'se chama'+obj.nome)

let triple = (n) =>{
    return n *3
}
let m = triple
console.log(m(4))

let me ={nome: 'marcelo' , sobrenome:'nassar'}

let teteu = {
    nome: 'matheus' ,
    apelido: 'maradona',
    idade: 20 ,
    sabedoria:10
}
let gx = (objeto) =>{
    let {nome, apelido} = objeto
    let {idade,sabedoria} = objeto
    let x = idade +sabedoria
    console.log(`o ${nome}, vulgo ${apelido} possui o coeficiente de inteligencia de ${x}`)
}
gx(teteu)


let animais = [
    {nome: 'Fluff', especie: 'Coelho'},
    {nome: 'kaka', especie: 'cachorro'},
    {nome: 'jaoo', especie: 'cachorro'},
    {nome: 'tuf', especie: 'Coelho'},
    {nome: 'juff', especie: 'peixe'},
    {nome: 'kluff', especie: 'peixe'}
]
let cachorros = animais.filter((animal) => {return animal.especie == 'cachorro'})
 console.log(cachorros)

 let newArray = animais.map(({nome,especie}) =>{return`${nome} e um ${especie}`})
 console.log(newArray)

 let displa = animais.reduce((acc,{nome})=>{ return `${nome} ${acc}`} ,'sao amigos') 
console.log(displa)