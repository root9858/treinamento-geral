const url = 'https://in-api-treinamento.herokuapp.com/posts/';

let $ = document;
let elemenPai = $.querySelector('.texto'); 


fetch(url).then(resp => {resp.json().then(res => { res.forEach(element => {
    let elementFilho = $.createElement("li")
    let {name, message} = element // descontruindo o element para name e message para poder usar depois
    elementFilho.innerText = `${name} e a mensagem e ${message}`
    elemenPai.appendChild(elementFilho)
    
});

}).catch(console.log)
})

