const url = 'https://in-api-treinamento.herokuapp.com/posts'

let botao = document.querySelector("#botao")
botao.addEventListener('click',(e) => {
    e.preventDefault()
    let nome = document.querySelector('#nome').value;
    let mensagem = document.querySelector("#mensagem").value;

    let novoPost ={
        'post': {
            'name': nome ,
            'message': mensagem
        }
    }

    console.log(nome)
    console.log(mensagem)
    
    let ObjConfig = {
        method: 'POST' ,
        headers: {'content-Type' : 'application/json'}, // criara obijeto de configuracao para poder mandar a requisicao
        body: JSON.stringify(novoPost)
    }


    fetch( url, ObjConfig)

    .then( resposta => {
        return resposta.json()
    } )
    .then(kk => {
        return kk.name
    })
    .then(console.log)

    /*
    .then(console.log)
    */
})